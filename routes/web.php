<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//fronten
Route::get('/','HomeController@index');
Route::get('/trang-chu','HomeController@index');

//danh sản phẩm trang chủ
Route::get('/danh-muc-san-pham/{category_product_id}','CategoryProductController@show_category_home');
Route::get('/danh-muc-thuong-hieu/{brand_product_id}','BrandController@show_brand_home');
//chitiet sản phẩm trang chủ
Route::get('/chi-tiet-san-pham/{product_id}','ProductController@detail_product');



//backen
Route::get('/admin','AdminController@index');
Route::get('/dashboard','AdminController@show_dashboard');
Route::get('/logout','AdminController@logout');
Route::post('/admin-dashboard','AdminController@dashboard');

//Category-Product
Route::get('/add-category-product','CategoryProductController@add_cateProduct');

Route::get('/edit-category-product/{category_product_id}','CategoryProductController@edit_category_product');
Route::get('/delete-category-product/{category_product_id}','CategoryProductController@delete_cate_product');

Route::get('/show-category-product','CategoryProductController@show_cateProduct');
Route::get('/unactive-category-product/{categoryp_roduct_id}','CategoryProductController@unactive_category_product');
Route::get('/active-category-product/{category_product_id}','CategoryProductController@active_category_product');

Route::post('/save-cate-product','CategoryProductController@save_cateProduct');
Route::post('/update-cate-product/{categoryp_roduct_id}','CategoryProductController@update_cateproduct');

//Brand-Product

Route::get('/add-brand-product','BrandController@add_brand_product');
Route::post('/save-brand-product','BrandController@save_brand_product');

Route::get('/edit-brand-product/{brand_product_id}','BrandController@edit_brand_product');
Route::get('/delete-brand-product/{brand_product_id}','BrandController@delete_brand_product');

Route::get('/show-brand-product','BrandController@show_brand_product');
Route::get('/unactive-brand-product/{brand_product_id}','BrandController@unactive_brand_product');
Route::get('/active-brand-product/{brand_product_id}','BrandController@active_brand_product');

Route::post('/update-brand-product/{brand_product_id}','brandController@update_brand_product');


//Product

Route::get('/add-product','ProductController@add_product');
Route::post('/save-product','ProductController@save_product');

Route::get('/edit-product/{product_id}','ProductController@edit_product');
Route::get('/delete-product/{roduct_id}','ProductController@delete_product');

Route::get('/show-product','ProductController@show_product');
Route::get('/unactive-product/{product_id}','ProductController@unactive_product');
Route::get('/active-product/{product_id}','ProductController@active_product');

Route::post('/update-product/{product_id}','ProductController@update_product');


///Cart
Route::post('/save-cart','CartController@save_cart');
Route::post('/update-cart-qty','CartController@update_cart_qty');
Route::get('/show-cart','CartController@show_cart');
Route::get('/delete-to-cart/{rowid}','CartController@delete_to_cart');


///checkout
Route::get('/login-checkout','CheckoutController@login_checkout');
Route::get('/logout-checkout','CheckoutController@logout_checkout');
Route::post('/add-customer','CheckoutController@add_customer');
Route::post('/login-customer','CheckoutController@login_customer');
Route::get('/checkout','CheckoutController@checkout');
Route::post('/save-checkout-customer','CheckoutController@save_checkout_customer');
Route::get('/payment','CheckoutController@payment');

//
