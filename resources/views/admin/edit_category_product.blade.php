@extends('admin_layout');
@section('admin_content')

<div class="row">
            <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            CẬP NHẬT DANH MỤC SẢN PHẨM
                        </header>
                       
                        <?php
                        $message =Session::get('message');
                        if($message){
                          echo '<span class="text-alert">'.$message.'</span>';
                          Session::put('message',null);
                        }
                      ?> 
                      <div class="panel-body">
                          @foreach($edit_category_product as $key => $edit_value)
                            <div class="position-center">
                              
                                <form role="form" action="{{URL::to('/update-cate-product/'.$edit_value->category_id)}}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên Danh mục sản phẩm</label>
                                    <input type="text" value="{{$edit_value->category_name}}" class="form-control"name ="name_cate_product" id="exampleInputEmail1" placeholder="Nhập tên danh mục sản phẩm">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Chi tiết danh mục sản phẩm</label>
                                    <textarea   value="" 
                                      style = "resize: none" rows = 6 class="form-control" name ="desc_cate_product" id="exampleInputPassword1" >{{$edit_value->category_desc}}</textarea>
                                </div>
                               
                              
                                @endforeach
                                <button type="submit" class="btn btn-info">Cập nhật danh mục</button>
                            </form>
                            </div>
                        </div>
                    </section>

            </div>
@endsection        