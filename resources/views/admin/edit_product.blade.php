@extends('admin_layout');
@section('admin_content')

<div class="row">
            <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            THÊM SẢN PHẨM
                        </header>
                        <div class="panel-body">
                        <?php
                        $message =Session::get('message');
                        if($message){
                          echo '<span class="text-alert">'.$message.'</span>';
                          Session::put('message',null);
                        }
                      ?>
                            <div class="position-center">
                                @foreach($edit_product as $key =>$pro) 
                                <form role="form" action="{{URL::to('/update-product/'.$pro->product_id)}}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên sản phẩm</label>
                                    <input type="text" class="form-control"name ="name_product"
                                    value="{{$pro->product_name}}">
                                </div>
                                <div class="form-group">
                                <label class="col-sm-3 control-label col-lg-3" for="inputSuccess">Thương hiệu</label>
                                <select name="brand_id" class="form-control m-bot15">
                                    @foreach ($brand_product as $key =>$brand)
                                    @if($brand->brand_id==$pro->brand_id)
                                         <option selected  value="{{$brand->brand_id}}">{{$brand->brand_name}}</option>
                                    @else
                                         <option value="{{$brand->brand_id}}">{{$brand->brand_name}}</option>
                                    @endif
                                    @endforeach
                               </select>
                               <div class="form-group">
                                <label class="col-sm-3 control-label col-lg-3" for="inputSuccess">Danh mục</label>
                                <select name="category_id" class="form-control m-bot15">
                                    @foreach ($cate_product as $key =>$cate)
                                    @if($cate->category_id==$pro->category_id)
                                         <option selected  value="{{$cate->category_id}}">{{$cate->category_name}}</option>
                                    @else
                                         <option value="{{$cate->category_id}}">{{$cate->category_name}}</option>
                                    @endif
                                    @endforeach
                               </select>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Chi tiết thương hiệu sản phẩm</label>
                                    <textarea type="text" style =  "resize: none" rows = 6
                                     class="form-control" name ="desc_product"    >{{$pro->product_desc}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> Giá sản phẩm</label>
                                    <input type="text" class="form-control"name ="price_product" value="{{$pro->product_price}}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Hình ảnh sản phẩm</label>
                                    <input type="file" class="form-control"name ="product_image" >
                                    <img src="{{URL::to('public/upload/product/'.$pro->product_image)}}" height="100px" width="100px" alt="">
                                </div>
                      
                                <div class="form-group">
                                <label class="col-sm-3 control-label col-lg-3" for="inputSuccess">Hiển thị</label>
                                <select name="product_status" class="form-control m-bot15">
                                <option value="0">Ẩn</option>
                                <option value="1">Hiển thị</option>
                               </select>
                              </div>
                                <button type="submit" class="btn btn-info">Chỉnh sửa sản phẩm</button>
                            </form>
                            @endforeach
                            </div>

                        </div>
                    </section>

            </div>
@endsection        