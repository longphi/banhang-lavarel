@extends('admin_layout');
@section('admin_content')

<div class="row">
            <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            THÊM THƯƠNG HIỆU CHO SẢN PHẨM
                        </header>
                        <div class="panel-body">
                        <?php
                        $message =Session::get('message');
                        if($message){
                          echo '<span class="text-alert">'.$message.'</span>';
                          Session::put('message',null);
                        }
                      ?>
                            <div class="position-center">
                                <form role="form" action="{{URL::to('/save-brand-product')}}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tên thương hiệu sản phẩm</label>
                                    <input type="text" class="form-control"name ="name_brand_product" id="exampleInputEmail1" placeholder="Nhập tên danh mục sản phẩm">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Chi tiết thương hiệu sản phẩm</label>
                                    <textarea type="text" style =  "resize: none" rows = 6 class="form-control" name ="desc_brand_product" id="exampleInputPassword1" placeholder="Nhập chi tiết danh mục"></textarea>
                                </div>
                                <div class="form-group">
                                <label class="col-sm-3 control-label col-lg-3" for="inputSuccess">Hiển thị</label>
                                <select name="brand_product_status" class="form-control m-bot15">
                                <option value="0">Ẩn</option>
                                <option value="1">Hiển thị</option>
                               </select>
                              </div>
                                <button type="submit" class="btn btn-info">Thêm danh mục</button>
                            </form>
                            </div>

                        </div>
                    </section>

            </div>
@endsection        