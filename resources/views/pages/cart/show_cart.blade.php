@extends('welcome')
@section('content')

<div class="features_items">
<section id="cart_items">
	<!-- <div class="row"> -->
		<div class="container">	
			<div class="breadcrumbs">
				<ol class="breadcrumb">
					<li><a href="#">Trang chủ</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="row">

<div class="col-md-9">
			<div class="table-responsive cart_info">
				<?php
				$content =Cart::content();
				?>
				<!-- $content lấy ra dc tất cả các giá trị được thêm vào giỏ hàng -->
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Hình ảnh</td>
							<td class="description">Mô tả</td>
							<td class="price">Giá</td>
							<td class="quantity">Số lượng</td>
							<td class="total">Tổng</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
							
						@foreach($content as $v_content)
						<tr>
							<td class="cart_product">
								<a href=""><img width= 50px height =50px 
								src="{{URL::to('public/upload/product/'.$v_content -> options ->image)}}" alt="" />
</a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$v_content->name}}</a></h4>
								<p>Id sản phẩm: {{$v_content->id}}</p>
							</td>
							<td class="cart_price">
							
								<p>{{number_format($v_content->price,0,',','.'). 'VND'}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
								<form role="form" action="{{URL::to('/update-cart-qty')}}" method="post">
								{{ csrf_field() }}
								<input class="btn btn-default btn-sm" type="number" min=1 name="quantity" value="{{$v_content->qty}}" autocomplete="off" size="2">
									<input class="btn btn-default btn-sm" type="submit" name="update_qty" value="Cập nhập" >
									<input class="btn btn-default btn-sm" type="hidden" name="rowId_cart" value="{{$v_content->rowId}}" >
</form>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">
									<?php
										$sub_total = $v_content->price * $v_content->qty;
										echo number_format((float)$sub_total).''. 'VND';
									
									?>


								</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="{{URL::to('/delete-to-cart/'.$v_content->rowId)}}"><i class="fa fa-times"></i></a>
							</td>
						</tr>
					@endforeach
						
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->


	<section id="do_action">
		<div class="container">
			<div class="row">

			 <div class="col-md-9">
				<!-- <div class="col-sm-6"> -->
					<div class="total_area">
						<ul>
							<li>Tổng  <span>{{Cart::pricetotal(0, ',' , '.'). ' '. 'VND'}}</span></li>
							<li>Thuế <span>{{Cart::tax(0, ',' , '.'). ' '. 'VND'}}</span></li>
							<li>Phí vận chuyển <span>Free</span></li>
							<li>Thành tiền<span>{{Cart::total(0, ',' , '.'). ' '. 'VND'}}</span></li>
						</ul>
						<?php
									//lấy biến id kiếm tra giá trị id nêu bằng null thì login
									$customer_id = Session::get('customer_id');
									if($customer_id!=NULL){
									?>	
									
										<a class="btn btn-default check_out" href="{{URL::to('/checkout/')}}".>Thanh toán</a>

										<?php
									}else{
										?>
										
										<a class="btn btn-default check_out" href="{{URL::to('/login-checkout/')}}".>Thanh toán</a>

									<?php
									}
									?>
							<!-- <a class="btn btn-default update" href="">Update</a> -->
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->


</div>
</div>
@endsection