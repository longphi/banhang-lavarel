@extends('welcome')
@section('content')

<div class="features_items">
<section id="cart_items">
    <div class="container">
			<div class="row">

			 <div class="col-md-9">
             <div class="breadcrumbs">
				<ol class="breadcrumb">
					<li><a href="#">Trang chủ</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>

		
			<div class="shopper-informations">
				<div class="row">
					<div class="col-md-10">
						<div class="shopper-info">
							<p>Shopper Information</p>
                            
							<form action="{{URL::to('/save-checkout-customer')}}" method="post" >
                            {{ csrf_field() }}
								<input type="text" name ="shipping_email"  placeholder="Email">
								<input type="text" name ="shipping_name" placeholder="Họ và tên">
								<input type="text" name ="shipping_address" placeholder="Địa chi">
								<input type="number"name ="shipping_phone"  placeholder="Điện thoại">
                                <textarea name="shipping_note"  placeholder="Ghi chú đơn hàng vận chuyển" rows="16"></textarea>
                                <input type="submit" value="Gửi" class="btn btn-primary btn-md">
									</input>	
                            </form>
							<!-- <a class="btn btn-primary" href="">Get Quotes</a>
							<a class="btn btn-primary" href="">Continue</a> -->
						</div>
					</div>
				
					<!-- <div class="col-sm-6">
						<div class="order-message">
							<p>Ghi chú đơn hàng</p>
							<textarea name="message"  placeholder="Notes about your order, Special Notes for Delivery" rows="16"></textarea>
							<label><input type="checkbox"> Shipping to bill address</label>
						</div>	
					</div>					 -->
				</div>
			</div>
			<div class="review-payment">
				<h2>Xem lại giỏ hàng</h2>
			</div>

			
			<div class="payment-options">
					<span>
						<label><input type="checkbox"> Direct Bank Transfer</label>
					</span>
					<span>
						<label><input type="checkbox"> Check Payment</label>
					</span>
					<span>
						<label><input type="checkbox"> Paypal</label>
					</span>
				</div>
		</div>
	</section> <!--/#cart_items-->



@endsection