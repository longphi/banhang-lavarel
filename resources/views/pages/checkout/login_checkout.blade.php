@extends('welcome')
@section('content')

<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
					<h2>Đăng nhập tài khoản</h2>
					<?php
                        $message =Session::get('message');
                        if($message){
                          echo '<span class="text-alert">'.$message.'</span>';
                          Session::put('message',null);}
                      ?>
						<form action="{{URL::to('/login-customer')}}" method="post">
						{{ csrf_field() }}
						<input type="text" name="customer_name" required ="Vui lòng nhập tên tài khoản " placeholder="Nhập tài khoản" />
						<input type="password" name="customer_pass" required ="Vui lòng nhập mật khẩu "placeholder="Mật khẩu"/>
							<span>
								<input type="checkbox" class="checkbox"> 
                                    Ghi nhớ mật khẩu
							</span>
							<button type="submit" class="btn btn-default">Dăng nhập</button>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">Hoặc</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Đăng ký tài khoản</h2>
						
						<form action="{{URL::to('/add-customer')}}" method="POST">
								{{ csrf_field() }}
						<input type="text" name="customer_name" required ="Vui lòng nhập tên tài khoản " placeholder="Nhập tài khoản" />
							<input type="email"  name="customer_email"required ="required " placeholder="Nhập email" />
							<input type="password" name="customer_pass" required ="Vui lòng nhập mật khẩu "placeholder="Mật khẩu"/>
							<input type="number" name="customer_phone"required ="Vui lòng nhập số điện thoại " placeholder="Điện thoại"/>
							<button type="submit" class="btn btn-default">Đăng ký</button>
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
	
	

@endsection