<?php

namespace App\Http\Controllers;
use DB;

use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();

class CategoryProductController extends Controller
{
      //function kiểm tra login
      public function AuthuLogin(){
        $admin_id = Session::get('admin_id');
        if($admin_id){
            return Redirect::to('dashboard');
        }else{
            Redirect::to('admin')->send();

        }
    }
    //
    public function add_cateProduct(){
        $this->AuthuLogin();
        return view('admin.add_category_product');
    }
    public function show_cateProduct(){
        $this->AuthuLogin();
        $all_cate_product = DB::table('tbl_category_product')->get();
        $manager_cate_product =view('admin.show_category_product')->with('all_category_product',$all_cate_product);
        return view('admin_layout')->with('admin.show_category_product', $manager_cate_product);

    }
    public function save_cateProduct(Request $request){
        $this->AuthuLogin();
        $data =array();
        $data['category_name'] =$request -> name_cate_product;
        $data['category_desc'] =$request -> desc_cate_product;
        $data['category_status'] =$request -> category_product_status;
        DB::table('tbl_category_product')->insert($data);
        Session::put('message','Thêm danh mục sản phẩm thành công');
        return Redirect::to ('show-category-product');
    }
    public function unactive_category_product($category_product_id){
        $this->AuthuLogin();
        DB::table('tbl_category_product')->where ('category_id',$category_product_id)->update(['category_status'=>1]);
        Session::put('message','Không kích hoạt danh mục sản phẩm thành công');       
        return Redirect::to ('show-category-product');
    }  
    public function active_category_product($category_product_id){
        $this->AuthuLogin();
        DB::table('tbl_category_product')->where ('category_id',$category_product_id)->update(['category_status'=>0]);
        Session::put('message','Kích hoạt danh mục sản phẩm thành công');       
        return Redirect::to ('show-category-product');
    }
    public function edit_category_product($category_product_id){
        $this->AuthuLogin();
        $edit_category_product = DB::table('tbl_category_product')->where('category_id',$category_product_id)->get();
        $manager_cate_product =view('admin.edit_category_product')->with('edit_category_product',$edit_category_product);
        return view('admin_layout')->with('admin.edit_category_product', $manager_cate_product);
}

//hàm update
public function update_cateproduct(Request $request, $category_product_id){
    $this->AuthuLogin();
    $data = array();
    $data['category_name'] =$request -> name_cate_product;
    $data['category_desc'] =$request -> desc_cate_product;
    DB::table('tbl_category_product')->where ('category_id',$category_product_id)->update($data);
    Session::put('message','Cập nhật danh mục sản phẩm thành công');       
    return Redirect::to ('show-category-product');

}
public function delete_cate_product ($category_product_id){
    $this->AuthuLogin();
    DB::table('tbl_category_product')->where ('category_id',$category_product_id)->delete();
    Session::put('message','Xóa danh mục sản phẩm thành công');       
    return Redirect::to ('show-category-product');
}

//end function admin page

//function home
public function show_category_home($category_product_id){

    $cate_product = DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','desc')->get();
    $brand_product= DB::table('tbl_brand_product')->where('brand_status','0')->orderby('brand_id','desc')->get();
    //lấy theo tên dnah mục
    $cate_name = DB::table('tbl_category_product')->where('tbl_category_product.category_id',$category_product_id)->limit(1)->get();
    
    $category_by_id = DB::table('tbl_product')->join('tbl_category_product','tbl_product.category_id','=','tbl_category_product.category_id')->where('tbl_product.category_id',$category_product_id)->get();
    return view('pages.category.show_category_home')->with('category',$cate_product)->with('brand',$brand_product)->with('category_by_id',$category_by_id)
    ->with('cate_name',$cate_name);

}
}