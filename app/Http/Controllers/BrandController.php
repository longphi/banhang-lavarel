<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();


class BrandController extends Controller
{
    //function kiểm tra login
    public function AuthuLogin(){
        $admin_id = Session::get('admin_id');
        if($admin_id){
            return Redirect::to('dashboard');
        }else{
            Redirect::to('admin')->send();

        }
    }
     //
     public function add_brand_product(){
        $this->AuthuLogin();
        return view('admin.add_brand_product');
    }
    public function show_brand_product(){
        $this->AuthuLogin();
        $all_brand_product = DB::table('tbl_brand_product')->get();
        $manager_brand_product =view('admin.show_brand_product')->with('all_brand_product',$all_brand_product);
        return view('admin_layout')->with('admin.show_brand_product', $manager_brand_product);

    }
    public function save_brand_product(Request $request){
        $this->AuthuLogin();
        $data =array();
        $data['brand_name'] =$request -> name_brand_product;
        $data['brand_desc'] =$request -> desc_brand_product;
        $data['brand_status'] =$request -> brand_product_status;
        DB::table('tbl_brand_product')->insert($data);
        Session::put('message','Thêm danh mục sản phẩm thành công');
        return Redirect::to ('show-brand-product');
    }
    public function unactive_brand_product($brand_product_id){
        $this->AuthuLogin();
        DB::table('tbl_brand_product')->where ('brand_id',$brand_product_id)->update(['brand_status'=>1]);
        Session::put('message','Không kích hoạt danh mục thương hiệu sản phẩm thành công');       
        return Redirect::to ('show-brand-product');
    }  
    public function active_brand_product($brand_product_id){
        $this->AuthuLogin();
        DB::table('tbl_brand_product')->where ('brand_id',$brand_product_id)->update(['brand_status'=>0]);
        Session::put('message','Kích hoạt danh mục thương hiệu sản phẩm thành công');       
        return Redirect::to ('show-brand-product');
    }
    public function edit_brand_product($brand_product_id){
        $this->AuthuLogin();
        $edit_brand_product = DB::table('tbl_brand_product')->where('brand_id',$brand_product_id)->get();
        $manager_brand_product =view('admin.edit_brand_product')->with('edit_brand_product',$edit_brand_product);
        return view('admin_layout')->with('admin.edit_brand_product', $manager_brand_product);
}

//hàm update
public function update_brand_product(Request $request, $brand_product_id){
    $this->AuthuLogin();
    $data = array();
    $data['brand_name'] =$request -> name_brand_product;
    $data['brand_desc'] =$request -> desc_brand_product;
    DB::table('tbl_brand_product')->where ('brand_id',$brand_product_id)->update($data);
    Session::put('message','Cập nhật danh mục sản phẩm thành công');       
    return Redirect::to ('show-brand-product');

}
public function delete_brand_product ($brand_product_id){
    $this->AuthuLogin();
    DB::table('tbl_brand_product')->where ('brand_id',$brand_product_id)->delete();
    Session::put('message','Xóa danh mục sản phẩm thành công');       
    return Redirect::to ('show-brand-product');
}
//end function admin page

//funtion show_bradnd_home
public function show_brand_home($brand_product_id){
    $cate_product =DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','desc')->get();
    //show danh mục sản phẩm từ bảng category_product  với điều kiện category_status băng 0 và sắp xép theo thứ tự mới add và dùng get() show ra
    $brand_product =DB::table('tbl_brand_product')->where('brand_status','0')->orderby('brand_id','desc')->get();
    $brand_name = DB::table('tbl_brand_product')->where('tbl_brand_product.brand_id',$brand_product_id)->limit(1)->get();
    
    $brand_by_id= DB:: table('tbl_product')->join('tbl_brand_product','tbl_product.brand_id','=','tbl_brand_product.brand_id')->where ('tbl_product.brand_id',$brand_product_id)->get();
    //tạo 1 biến gắn dữ liệu từ bảng sản phẩm join bảng tbl_brand_prodcut, và bảng product lấy ra brand_id bằng với brand_id của bảng tbl_brand_product điều kiện là bradn_id bằng với biến $brand_product_id
    return view('pages.brand.show_brand_home')->with('category',$cate_product)->with('brand',$brand_product)->with('brand_by_id',$brand_by_id)->with('brand_name',$brand_name);

}


}