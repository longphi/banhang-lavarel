<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();

class ProductController extends Controller
{
      //function kiểm tra login
      public function AuthuLogin(){
        $admin_id = Session::get('admin_id');
        if($admin_id){
            return Redirect::to('dashboard');
        }else{
            Redirect::to('admin')->send();

        }
    }

    // function add_product()
    public function add_product(){
        $this->AuthuLogin();
        $cate_product= DB::table('tbl_category_product')->orderby('category_id','desc')->get();
        $brand_product= DB::table('tbl_brand_product')->orderby('brand_id','desc')->get();
        return view('admin.add_product')->with('cate_product',$cate_product)->with('brand_product',$brand_product);
    }    

    //function show_product()
    public function show_product(){
        $this->AuthuLogin();
        $show_product = DB::table('tbl_product')
        ->join('tbl_category_product','tbl_category_product.category_id','=','tbl_product.category_id')
        ->join('tbl_brand_product','tbl_brand_product.brand_id','=','tbl_product.brand_id')
        ->orderby('tbl_product.product_id','desc')->get();
        $manager_product = view('admin.show_product')->with('show_product',$show_product);
        return view('admin_layout')->with('admin.show_product',$manager_product);
    }

    //funciton save_product()
    public function save_product(Request $request){
        $this->AuthuLogin();
        $data= array();
        $data['product_name'] =$request -> name_product;
        $data['product_desc']=$request-> desc_product;
        $data['product_price'] = $request->price_product;
        $data['product_status'] = $request->product_status;
        $data['product_content'] = $request ->product_content;
        $data['brand_id'] =$request->brand_id;
        $data['category_id'] =$request->category_id;
        $data['product_image'] =$request->product_image;
          
        //add file ảnh
           $get_image = $request->file('product_image');
        if($get_image){
            //lấy tên chuỗi đâu
            $get_name_image= $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));//phân tách chuỗi dựa vào dấu .
            $new_image =$name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();//lấy đuôi mở rộng
            $get_image->move('public/upload/product',$new_image);
            $data['product_image'] =$new_image;
            DB::table('tbl_product')->insert($data);
            Session::put('message','Thêm sản phẩm thành công');
            return Redirect::to ('show-product');
        }
        $data['product_image']='';
         DB::table('tbl_product')->insert($data);
        Session::put('message','Thêm sản phẩm thành công');
        return Redirect::to ('show-product');
    }


//function update 
public function update_product(Request $request, $product_id ){
    $this->AuthuLogin();    
$data =array();
 $data['product_name'] =$request -> name_product;
 $data['product_desc']=$request-> desc_product;
 $data['product_price'] = $request->price_product;
 $data['product_status'] = $request->product_status;
 $data['brand_id'] =$request->brand_id;
 $data['category_id'] =$request->category_id;
//  $data['product_image'] =$request->product_image;
  $get_image =$request->file('product_image')  ;
  if($get_image){
    //lấy tên chuỗi đâu
    $get_name_image= $get_image->getClientOriginalName();
    $name_image = current(explode('.',$get_name_image));//phân tách chuỗi dựa vào dấu .
    $new_image =$name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();//lấy đuôi mở rộng
    $get_image->move('public/upload/product',$new_image);
    $data['product_image'] =$new_image;
    DB::table('tbl_product')->where('product_id',$product_id)->update($data);
    Session::put('message','Cập nhật sản phẩm thành công');
    return Redirect::to ('show-product');
}
// $data['product_image']='';
DB::table('tbl_product')->where('product_id',$product_id)->update($data);
Session::put('message','Thêm sản phẩm thành công');
return Redirect::to ('show-product');
}

//function edit
public function edit_product ($product_id){
    $this->AuthuLogin();
    $cate_product= DB::table('tbl_category_product')->orderby('category_id','desc')->get();
    $brand_product= DB::table('tbl_brand_product')->orderby('brand_id','desc')->get();
    $edit_product = DB::table('tbl_product')->where('product_id',$product_id)->get();
    $manager_product =view('admin.edit_product')->with('edit_product',$edit_product)->with('cate_product',$cate_product)
    ->with('brand_product',$brand_product);
    return view('admin_layout')->with('admin.edit_product', $manager_product);
}



//funcition active/unactive
public function active_product($product_id){
    $this->AuthuLogin();
    DB::table('tbl_product')->where ('product_id',$product_id)->update(['product_status'=>0]);
    Session::put('message','Kích hoạt danh mục thương hiệu sản phẩm thành công');       
    return Redirect::to ('show-product');
}
public function unactive_product($product_id){
    $this->AuthuLogin();
    DB::table('tbl_product')->where ('product_id',$product_id)->update(['product_status'=>1]);
    Session::put('message','Kích hoạt danh mục thương hiệu sản phẩm thành công');       
    return Redirect::to ('show-product');
}

//function delele
public function delete_product ($product_id){
    $this->AuthuLogin();
    DB::table('tbl_product')->where ('product_id',$product_id)->delete();
    Session::put('message','Xóa sản phẩm thành công');       
    return Redirect::to ('show-product');
}
//end backen

//frontend

//fuction chi tiết sản phẩm

public function detail_product($product_id){
    $cate_product = DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','desc')->get();
    $brand_product= DB::table('tbl_brand_product')->where('brand_status','0')->orderby('brand_id','desc')->get();
    $details_product = DB::table('tbl_product')
    ->join('tbl_category_product','tbl_category_product.category_id','=','tbl_product.category_id')
    ->join('tbl_brand_product','tbl_brand_product.brand_id','=','tbl_product.brand_id')
    ->where('tbl_product.product_id',$product_id)->get();

    //lấy các sản phẩm liên quan dựa vào danh mục sản phẩm
    foreach ($details_product as $key => $values){
     $category_id = $values->category_id;
    }
    $related_product = DB::table('tbl_product')
    ->join('tbl_category_product','tbl_category_product.category_id','=','tbl_product.category_id')
    ->join('tbl_brand_product','tbl_brand_product.brand_id','=','tbl_product.brand_id')
    ->where('tbl_category_product.category_id',$category_id)->whereNotIn('tbl_product.product_id',[$product_id]) ->get();
//lấy các sản phẩm thuộc danh mục trừ ra cái product_id đã lấy dc
    return view('pages.product.show_product_detail')->with('category',$cate_product)->with('brand',$brand_product)->with('product_details',$details_product)
    ->with('category_related',$related_product);
}


}
